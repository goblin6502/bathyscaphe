//
//  CMRThreadFileLoadingTask.h
//  BathyScaphe
//
//  Updated by Tsutomu Sawada on 12/10/08.
//  Copyright 2005-2012 BathyScaphe Project. All rights reserved.
//  encoding="UTF-8"
//

#import <Foundation/Foundation.h>
#import "CMRThreadLayoutTask.h"
#import "CMRThreadComposingTask.h"


@interface CMRThreadFileLoadingTask : CMRThreadComposingTask {
	@private
	NSString *_filepath;
}

+ (id)taskWithFilepath:(NSString *)filepath;
- (id)initWithFilepath:(NSString *)filepath;

- (NSString *)filepath;
- (void)setFilepath:(NSString *)aFilepath;
@end
