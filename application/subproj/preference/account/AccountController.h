//
//  AccountController.h
//  BathyScaphe
//
//  Updated by Tsutomu Sawada on 07/11/21.
//  Copyright 2005-2013 BathyScaphe Project. All rights reserved.
//  encoding="UTF-8"
//

#import <Cocoa/Cocoa.h>
#import "PreferencesController.h"

@interface AccountController : PreferencesController
{
    IBOutlet NSTextField *x2chAccountField;
    IBOutlet NSTextField *be2chAccountField;
    IBOutlet NSTextField *p22chAccountField;

    IBOutlet NSSecureTextField *x2chPasswordField;
    IBOutlet NSSecureTextField *be2chPasswordField;
    IBOutlet NSSecureTextField *p22chPasswordField;
    
    BOOL x2chPasswordFilled;
    BOOL be2chPasswordFilled;
    BOOL p22chPasswordFilled;
}

- (IBAction)accountChanged:(id)sender;
- (IBAction)passwordChanged:(id)sender;
@end
